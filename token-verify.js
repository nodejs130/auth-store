const jwt = require('jsonwebtoken');

const secret = 'TheSecret';

function verifyToken(token, secret) {
  return jwt.verify(token, secret);
}

const token =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInJvbGUiOiJjdXN0b21lciIsImlhdCI6MTY3Nzc2MTA0Mn0.RMJF9L1EY6CYT1tafxGzTLp1oxLE8JLuA_2WuV6Gz74';
const payload = verifyToken(token, secret);
console.log(payload);
